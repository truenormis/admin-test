# Admin - Test Documentation

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/truenormis/admin-test.git
   ```

2. Copy the `.env.example` file to `.env`:

   ```bash
   cp .env.example .env
   ```

3. Build the Docker containers:

   ```bash
   docker-compose build
   ```

4. Start the application in detached mode:

   ```bash
   docker-compose up -d
   ```

5. Access the application container:

   ```bash
   docker exec -it admin-test-app bash
   ```

6. Run migrations and seed the database within the container:

   ```bash
   php artisan migrate --seed
   ```

## Usage

### Login

Make a GET request to the following endpoint to log in with the provided credentials:

```bash
GET /login
```

- `admin@admin.com` is login.
-  `password` is password.

### Admin Dashboard

To access the admin dashboard, make a GET request to the following endpoint:

```bash
GET /admin
```
