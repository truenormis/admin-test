<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use App\Services\EmployeeService;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function __construct(private EmployeeService $service)
    {
    }

    public function index()
    {
        $employees = Employee::paginate(10);
        $heads = $this->service->getHeads();
        $config = $this->service->getConfig($employees);
        return view('employees')->with(['heads' => $heads,'config' => $config,'employees' => $employees]);
    }


    public function byCompany(Company $company)
    {
        $employees = $company->employees()->paginate(10);
        $heads = $this->service->getHeads();
        $config = $this->service->getConfig($employees);
        return view('employees')->with(['heads' => $heads,'config' => $config,'employees' => $employees]);
    }


    public function store(EmployeeRequest $request)
    {
        $validated = $request->validated();
        //dd($validated);
        $employee = Employee::create([
            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'company_id' => Company::findOrFail($validated['company_id'])->id,
            'email' => $validated['email'],
            'phone' => $validated['phone'],
        ]);
        return redirect()->route('admin.employee.index');
    }


    public function add()
    {
        return view('employees.add');
    }
    public function edit(Employee $employee)
    {
        return view('employees.edit')->with(['employee' => $employee]);
    }

    public function update(EmployeeRequest $request, Employee $employee)
    {
        $validated = $request->validated();
        $employee->update([
            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'company_id' => Company::findOrFail($validated['company_id'])->id,
            'email' => $validated['email'],
            'phone' => $validated['phone'],
        ]);
        return redirect()->route('admin.employee.index');
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        return back();
    }
}
