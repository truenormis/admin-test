<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use App\Models\Logo;
use App\Services\CompaniesService;
use App\Services\LogoService;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct(private CompaniesService $service)
    {
    }

    public function index()
    {
        $companies = Company::paginate(10);
        $heads = $this->service->getHeads();
        $config = $this->service->getConfig($companies);
        return view('companies')->with(['heads' => $heads,'config' => $config,'companies' => $companies]);
    }


    public function add()
    {
        return view('companies.add');
    }

    public function edit(Company $company)
    {
        return view('companies.edit')->with(['company' => $company]);
    }
    public function store(CompanyRequest $request)
    {
        $validated = $request->validated();
        $company = Company::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'website' => $validated['website'],
        ]);
        $url = $request->hasFile('file') ? LogoService::getUrl($request->file('file')) : config('app.default_logo');
        Logo::create([
            'url' => $url,
            'company_id' => $company->id
        ]);
        return redirect()->route('admin.companies');
    }



    public function update(CompanyRequest $request, Company $company)
    {
        $validated = $request->validated();
        $company->update([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'website' => $validated['website'],
        ]);
        $url = $request->hasFile('file') ? LogoService::getUrl($request->file('file')) : $company->logo->url;
        $company->logo->update([
            'url' => $url,
            'company_id' => $company->id
        ]);
        return redirect()->route('admin.companies');
    }

    public function destroy(Company $company)
    {
        $company->employees->each->delete();
        $company->delete();

        return back();
    }
}
