<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Logo extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'company_id',
    ];

    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }
}
