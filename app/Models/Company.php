<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Company extends Model
{
    use HasFactory;

    protected $guarded = false;
    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class);
    }
    public function logo(): HasOne
    {
        return $this->hasOne(Logo::class);
    }
}
