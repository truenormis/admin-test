<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class LogoService
{

    public static function getUrl(UploadedFile $file)
    {
        $name = $file->hashName();
        $path = 'images';
        Storage::disk('public')->put($path,$file);
        return asset('storage'.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$name);
    }
}
