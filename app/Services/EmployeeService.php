<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Employee;
use Ramsey\Collection\Collection;

class EmployeeService
{
    private array $heads;

    public function __construct()
    {
        $this->heads = [
            'ID',
            'Full Name',
            'Company Name',
            'Email',
            'Phone',
            ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];

    }

    /**
     * @param $employees
     * @param array $config
     * @return array
     */
    private function getData($employees): array
    {
        $data = [];
        foreach ($employees as $employee) {
            $data[] = [
                $employee->id,
                $employee->first_name.' '.$employee->last_name,
                "<a href='".route('admin.employee.by-company',['company' => $employee->company->id])."'>".$employee->company->name."</a>",
                $employee->email,
                $employee->phone,
                $this->getBtn($employee)];
        }
        return $data;
    }

    /**
     * @param Company $employee
     * @return string
     */
    private function getBtn(Employee $employee): string
    {
        $routes = [
            'edit' => route('admin.employee.edit',['employee' => $employee]),
            'delete' => route('admin.employee.destroy', ['employee' => $employee]),
        ];
        return view('buttons')->with(['routes' => $routes]);
    }

    public function getConfig($companies): array
    {

        $config = [
            'data' => $this->getData($companies),
            'order' => [[1, 'asc']],
            'columns' => [null, null, null, ['orderable' => false]],
        ];
        return $config;
    }

    /**
     * @return array
     */
    public function getHeads(): array
    {
        return $this->heads;
    }
}
