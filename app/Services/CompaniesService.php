<?php

namespace App\Services;

use App\Models\Company;
use Ramsey\Collection\Collection;

class CompaniesService
{
    private array $heads;

    public function __construct()
    {
        $this->heads = [
            'ID',
            'Name',
            'Logo',
            ['label' => 'Email', 'width' => 15],
            'Website',
            ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];

    }

    /**
     * @param $companies
     * @param array $config
     * @return array
     */
    private function getData($companies): array
    {
        $data = [];
        foreach ($companies as $company) {
            $data[] = [$company->id, $company->name, $this->getImage($company), $company->email,"<a href='$company->website'>$company->website</a>", $this->getBtn($company)];
        }
        return $data;
    }

    private function getImage(Company $company): string
    {

        return "<a href='".$company->logo->url."' target='_blank'><img src='".$company->logo->url."' alt='Company Logo' style='width: 50px;'></a>";

    }

    /**
     * @param Company $company
     * @return string
     */
    private function getBtn(Company $company): string
    {
        $routes = [
            'edit' => route('admin.companies.edit',['company' => $company]),
            'delete' => route('admin.companies.destroy',['company' => $company]),
            'info' => route('admin.employee.by-company',['company' => $company])
        ];
        return view('buttons')->with(['routes' => $routes]);
    }

    public function getConfig($companies): array
    {

        $config = [
            'data' => $this->getData($companies),
            'order' => [[1, 'asc']],
            'columns' => [null, null, null, ['orderable' => false]],
        ];
        return $config;
    }

    /**
     * @return array
     */
    public function getHeads(): array
    {
        return $this->heads;
    }
}
