<?php


use App\Http\Controllers\AdminController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function (){
    return redirect()->route('admin.dashboard');
});


Route::prefix('admin')->middleware(['auth', 'verified'])->name('admin.')->group(function (){
    Route::get('/', AdminController::class)->name('dashboard');

    Route::get('/companies', [CompanyController::class,'index'])->name('companies');
    Route::get('/companies/add', [CompanyController::class,'add'])->name('companies.add');
    Route::post('/companies', [CompanyController::class,'store'])->name('companies.store');
    Route::get('/companies/edit/{company}', [CompanyController::class,'edit'])->name('companies.edit');
    Route::put('/companies/{company}', [CompanyController::class,'update'])->name('companies.update');
    Route::delete('/companies/{company}', [CompanyController::class,'destroy'])->name('companies.destroy');

    Route::get('/employees', [EmployeeController::class,'index'])->name('employee.index');
    Route::post('/employees', [EmployeeController::class,'store'])->name('employee.store');
    Route::get('/employees/edit/{employee}', [EmployeeController::class,'edit'])->name('employee.edit');
    Route::get('/employees/add', [EmployeeController::class,'add'])->name('employee.add');
    Route::get('/employees/{company}', [EmployeeController::class,'byCompany'])->name('employee.by-company');
    Route::put('/employees/{employee}', [EmployeeController::class,'update'])->name('employee.update');
    Route::delete('/employees/{employee}', [EmployeeController::class,'destroy'])->name('employee.destroy');



});


//Route::get('/admin/companies', [CompanyController::class,'index'])->name('admin');

//Route::middleware('auth')->group(function () {
//    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
//});

require __DIR__.'/auth.php';
