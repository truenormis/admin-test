<nobr>
    <a href='{{$routes['edit']}}'>
        <button class='btn btn-xs btn-default text-primary mx-1 shadow' title='Edit'>
            <i class='fa fa-lg fa-fw fa-pen'></i>
        </button>
    </a>
    <form method='post' action='{{$routes['delete']}}'>
        @csrf
        @method('DELETE')
        <button class='btn btn-xs btn-default text-danger mx-1 shadow' title='Delete'>
            <i class='fa fa-lg fa-fw fa-trash'></i>
        </button>
    </form>
    @if(isset($routes['info']))
        <a href="{{$routes['info']}}">
            <button class="btn btn-xs btn-default text-teal mx-1 shadow" title="Details">
                <i class="fa fa-lg fa-fw fa-eye"></i>
            </button>
        </a>
    @endif


</nobr>
