@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Add Company</h1>
@stop

@section('content')
    {{-- Minimal --}}
    <form action="{{route('admin.companies.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <x-adminlte-input name="name" label="Name" placeholder="Name" fgroup-class="col-md-6"/>
        <x-adminlte-input name="email" label="Email" placeholder="email@email.com" type="email" fgroup-class="col-md-6"/>
        <x-adminlte-input name="website" label="Website" placeholder="example.com" fgroup-class="col-md-6"/>
        <x-adminlte-input-file name="file" igroup-size="sm" placeholder="Choose a file..." fgroup-class="col-md-6">
            <x-slot name="prependSlot">
                <div class="input-group-text bg-lightblue">
                    <i class="fas fa-upload"></i>
                </div>
            </x-slot>
        </x-adminlte-input-file>
        <x-adminlte-button class="btn-flat" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save" />
    </form>



@stop

@section('css')

@stop


