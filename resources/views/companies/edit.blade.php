@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit Company</h1>
@stop

@section('content')
    {{-- Minimal --}}
    <form action="{{route('admin.companies.update',['company' => $company])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-adminlte-input name="name" label="Name" placeholder="Name" fgroup-class="col-md-6" value="{{$company->name}}"/>
        <x-adminlte-input name="email" label="Email" placeholder="email@email.com" type="email" fgroup-class="col-md-6" value="{{$company->email}}"/>
        <x-adminlte-input name="website" label="Website" placeholder="example.com" fgroup-class="col-md-6" value="{{$company->website}}"/>
        <x-adminlte-input-file name="file" igroup-size="sm" placeholder="Choose a file..." fgroup-class="col-md-6">
            <x-slot name="prependSlot">
                <div class="input-group-text bg-lightblue">
                    <i class="fas fa-upload"></i>
                </div>
            </x-slot>
        </x-adminlte-input-file>
        <x-adminlte-button class="btn-flat" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save" />
    </form>



@stop

@section('css')

@stop


