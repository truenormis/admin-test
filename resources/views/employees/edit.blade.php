@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit Employee <b>{{$employee->first_name}} {{$employee->last_name}}</b></h1>
@stop

@section('content')
    {{-- Minimal --}}
    <form action="{{route('admin.employee.update',['employee' => $employee])}}" method="post">
        @csrf
        @method('PUT')
        <x-adminlte-input name="first_name" label="First Name" placeholder="Jone" fgroup-class="col-md-6" value="{{$employee->first_name}}"/>
        <x-adminlte-input name="last_name" label="Last Name" placeholder="Doe" fgroup-class="col-md-6" value="{{$employee->last_name}}"/>
        @php
            $companies = \App\Models\Company::all();
            $options = [];
            foreach ($companies as $company){
                $options[$company->id] = $company->name;
            }
    //        $options->toArray();
        @endphp
        <x-adminlte-select name="company_id" fgroup-class="col-md-6">
            <x-adminlte-options :options="$options" selected="{{$employee->company->id}}"/>
        </x-adminlte-select>
        <x-adminlte-input name="email" label="Email" placeholder="email@email.com" type="email"
                          fgroup-class="col-md-6" value="{{$employee->email}}"/>
        <x-adminlte-input name="phone" label="Phone Number" placeholder="+380 98 735 85 48" fgroup-class="col-md-6" value="{{$employee->phone}}"/>
        <x-adminlte-button class="btn-flat" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
    </form>



@stop

@section('css')

@stop


